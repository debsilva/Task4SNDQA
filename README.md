<br />
<p align="center">
  <h3 align="center">Task4 - Springer Nature Digital - QA Assessment</h3>

  <p>
  This is a test project developed for the Springer Nature Digital (SND) QA Assessment. This is the task 4 and its objective is the following:
<br />
<br />
<table><tr><th>
Create a user journey test using any specific language and tools of your preference to validate the search functionality

Push your code to a VCS of your choice (BitBucket, GitHub, etc.) with small commits and clear commit messages. Also justify your choice of tools.
</th></tr></table>
<br />
<br />
This project had been developed using Cucumber and Java. And the reason I choose those technologies is because, in my understanding, those are the technologies used in SND. The technologies versions were chosen by the newest and stable ones, when not conflicting.

The versions used for creating and executing this projects are:
<ul>
<li>openjdk 11.0.9.1</li>
<li>git 2.17.1</li>
<li>cucumber-java 6.9.0</li>
<li>cucumber-junit 6.9.0</li>
<li>selenium-java 3.141.59</li>
<li>commons-io 2.6</li>
<li>Google Chrome 86.0.4240.183</li>
<li>ChromeDriver 86.0.4240.22</li>
<li>Apache Maven 3.6.3</li>
<li>Github</li>
</ul>
I don’t think I forgot any technology, and if I did, I ask you to tell me.

For executing this project, I recommend installing the technologies in the versions listed above.
For Chromedriver and Apache maven, those need to be set in the computer environment variable ($PATH).
    <br />
    <br />
    <br />
  </p>
</p>
