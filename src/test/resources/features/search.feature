
Feature: Search for contents as a not logged in user
	As a not logged in user
	I want to search for contents
	So that I can look for interesting articles that are available for me

Background: User is not logged In
	Given that I navigate to the search page
	When I uncheck the Include Preview-Only content option
	
	Scenario: As someone who wants to clean the search
		Given I enter "Rest API" in the Search field input
		When I click the New Search option
		Then I expect Search field to be cleaned
		
	Scenario Outline: As someone who wants to check if the search appears in the result list
		Given I enter <title> in the Search field input
		When I click the Search button
		Then I expect that the content with the exact title <title> is shown in the result list
	
	Examples: 
		| title 																				  		|
		| "Multi-core Symbolic Bisimulation Minimisation" 		|
		| "Encyclopedia of Database Systems" 									|
		| "Journal of Nanoparticle Research: looking forward" |

	Scenario Outline: As someone who wants to check if the search appears in the result list
			Given I enter <title> in the Search field input
			When I click the Search button
			And I click in the result title <title>
			Then I expect to be redirected to the <title> content page
	
	Examples: 
		| title 																				  													|
		| "Efficient simulation of bubble dispersion and resulting interaction" 		|
		| "Usefulness of prototypes in conceptual design: students’ view"  				  |
		| "Stabilization of high-plasticity silt using waste brick powder"					|
		