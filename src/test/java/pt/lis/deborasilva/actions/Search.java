package pt.lis.deborasilva.actions;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.Scenario;
import pt.lis.deborasilva.assets.ResultsList;

public class Search {
	private WebDriver driver;
	private WebElement newSearchOption;
	private WebElement previewOnlyCheckbox;
	private WebElement searchField;
	private WebElement refineSearch;
	private WebElement searchButtom;
	private ResultsList resultsList;

	public Search() {
		System.setProperty("webdriver.chrome.driver", "/home/debz/Drivers/chromedriver");
		setDriver(new ChromeDriver());

		getDriver().get("https://rd.springer.com/search");
		acceptCookies();

		setResultsList(new ResultsList(getDriver()));
	}

	public void acceptCookies() {
		WebElement acceptCookies = getDriver().findElement(By.id("onetrust-accept-btn-handler"));
		acceptCookies.click();
	}

	public void waitUntilPageLoads() {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(pageLoadCondition);
	}
	
	public void takeScreenshot(Scenario scenario) {
		File file = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(file, new File("target/screenshots/" + scenario.getId() + ".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public WebElement getSearchField() {
		setSearchField(getDriver().findElement(By.id("query")));
		return searchField;
	}

	public void setSearchField(WebElement search) {
		this.searchField = search;
	}

	public WebElement getNewSearchOption() {
		setNewSearchOption(getDriver().findElement(By.id("global-search-new")));
		return newSearchOption;
	}

	public void setNewSearchOption(WebElement newSearchOption) {
		this.newSearchOption = newSearchOption;
	}

	public WebElement getPreviewOnlyCheckbox() {
		setPreviewOnlyCheckbox(getRefineSearch().findElement(By.id("results-only-access-checkbox")));
		return previewOnlyCheckbox;
	}

	public void setPreviewOnlyCheckbox(WebElement previewOnlyCheckbox) {
		this.previewOnlyCheckbox = previewOnlyCheckbox;
	}

	public WebElement getRefineSearch() {
		setRefineSearch(getDriver().findElement(By.id("kb-nav--aside")));
		return refineSearch;
	}

	public void setRefineSearch(WebElement refineSearch) {
		this.refineSearch = refineSearch;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
		getDriver().manage().window().maximize();
		getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	public WebElement getSearchButtom() {
		setSearchButtom(getDriver().findElement(By.id("search")));
		return searchButtom;
	}

	public void setSearchButtom(WebElement searchButtom) {
		this.searchButtom = searchButtom;
	}

	public ResultsList getResultsList() {
		return resultsList;
	}

	public void setResultsList(ResultsList resultsList) {
		this.resultsList = resultsList;
	}

}
