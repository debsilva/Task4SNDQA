package pt.lis.deborasilva.steps;

import org.junit.Assert;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pt.lis.deborasilva.actions.Search;
import pt.lis.deborasilva.assets.ContentPage;

public class PerformSearchSteps {
	private Search search;
	private ContentPage contentPage;

	@Given("that I navigate to the search page")
	public void thatINavigateToTheSearchPage() {
		search = new Search();
	}

	@Given("I enter {string} in the Search field input")
	public void iFillTheSearchFieldWith(String string) {
		search.getSearchField().sendKeys(string);
	}

	@When("I uncheck the Include Preview-Only content option")
	public void iUncheckTheIncludePreviewOnlyContentOption() {
		search.getPreviewOnlyCheckbox().click();
		search.waitUntilPageLoads();
	}

	@When("I click the New Search option")
	public void iCleanTheSearchClickingInNewSearch() {
		search.getNewSearchOption().click();
	}

	@When("I click the Search button")
	public void iClickTheSearchButton() {
		search.getSearchButtom().click();
	}

	@When("I click in the result title {string}")
	public void iClickInTheResultTitle(String title) {
		search.getResultsList().getResultItem(title).click();
		search.waitUntilPageLoads();
	}

	@Then("I expect Search field to be cleaned")
	public void theSearchFieldShouldBeCleaned() {
		Assert.assertEquals("", search.getSearchField().getText());
	}

	@Then("I expect that the content with the exact title {string} is shown in the result list")
	public void iExpectThatTheContentWithTheExactTitleIsShownInTheResultList(String title) {
		boolean exists = search.getResultsList().containsResultWithExactTitle(title);
		Assert.assertTrue(exists);
	}

	@Then("I expect to be redirected to the {string} content page")
	public void iExpectToBeRedirectedToTheContentPage(String title) {
		contentPage = new ContentPage(search.getDriver());
		Assert.assertEquals(title, contentPage.getContentTitle().getText());
	}

	@After(order = 1)
	public void screenshot(Scenario scenario) {
		search.takeScreenshot(scenario);
	}

	@After(order = 0)
	public void closeBrowser() {
		search.getDriver().quit();
	}

}
