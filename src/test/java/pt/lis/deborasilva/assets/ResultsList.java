package pt.lis.deborasilva.assets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ResultsList {
	private WebDriver driver;
	private WebElement resultsList;
	
	public ResultsList(WebDriver dr) {
		setDriver(dr);
	}
	
	public boolean containsResultWithExactTitle(String title) {
		boolean exists = getResultItem(title) != null;
		return exists;
	}
	
	public WebElement getResultItem(String title) {
		WebElement resultItem = getResultsList().findElement(By.linkText(title));
		return resultItem;
	}
	
	public WebElement getResultsList() {
		setResultsList(getDriver().findElement(By.id("results-list")));
		return resultsList;
	}
	public void setResultsList(WebElement resultsList) {
		this.resultsList = resultsList;
	}
	public WebDriver getDriver() {
		return driver;
	}
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
}
