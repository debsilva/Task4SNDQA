package pt.lis.deborasilva.assets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ContentPage {
	private WebElement contentTitle;
	private WebDriver driver;

	public ContentPage(WebDriver dr) {
		setDriver(dr);
		setContentTitle(getDriver().findElement(By.cssSelector("h1[data-test='article-title']")));
	}
	
	public WebElement getContentTitle() {
		return contentTitle;
	}

	public void setContentTitle(WebElement contentTitle) {
		this.contentTitle = contentTitle;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
}
